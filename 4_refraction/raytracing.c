#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <stdarg.h>
#include <stdio.h>
#include "raytracing.h"

/*微小距離*/
#define EPSILON (1.0f/512)
/*再帰呼出しの制限回数*/
#define MAX_RECURSION 8

int recursive_raytrace(const scene_t* scene, const ray_t *eye_ray, colorf_t *out_col, size_t recursion_level){
  if(recursion_level > MAX_RECURSION){ /*再帰呼出し回数が制限回数を超えた後*/
    return 0; /*何もしない*/
  } /* if */
  else{ /*再帰呼出し回数が制限回数以内の時*/
    int res; /*交差判定の結果*/
    shape_t *shape; /*交差した物体へのポインタ*/
    intersection_point_t intp; /*交点*/

    res = get_nearest_shape(scene, eye_ray, FLT_MAX, 0, &shape, &intp); /*交差判定*/
    if(res){ /*交差する物体がある場合*/
      colorf_t col = {0,0,0};
      size_t i;

      /*環境光の計算*/
      col.r = shape->material.ambient_ref.r * scene->ambient_illuminance.r;
      col.g = shape->material.ambient_ref.g * scene->ambient_illuminance.g;
      col.b = shape->material.ambient_ref.b * scene->ambient_illuminance.b;

      for(i = 0; i < scene->num_lights; ++i){
        const light_t* light = &scene->lights[i];
        vector_t light_dir; /*入射ベクトル*/
        float nl_dot; /*法線ベクトルと入射ベクトルの内積*/
        float dl; /*視線との交点と光源の距離*/

        if(light->type == LT_POINT){ /*点光源の場合*/
          /*入射ベクトルの計算*/
          light_dir.x = light->vector.x - intp.position.x;
          light_dir.y = light->vector.y - intp.position.y;
          light_dir.z = light->vector.z - intp.position.z;
          dl = norm(&light_dir) - EPSILON;
          normalize(&light_dir);
        }
        else if(light->type == LT_DIRECTIONAL){ /*並行光源の場合*/
          /*入射ベクトルの計算*/
          light_dir.x = light->vector.x;
          light_dir.y = light->vector.y;
          light_dir.z = light->vector.z;
          dl = 100000000; /*無限大を表す*/
          normalize(&light_dir);
        }

        /*完全鏡面反射する質感の場合*/
        if(shape->material.type == MT_PERFECT_REF){
          vector_t inv_eye_dir; /*視線ベクトルの逆ベクトル*/
          float vn_dot; /*視線ベクトルの逆ベクトルと法線ベクトルの内積*/

          /*視線ベクトルの逆ベクトルの計算*/
          inv_eye_dir.x = -1.0 * eye_ray->direction.x;
          inv_eye_dir.y = -1.0 * eye_ray->direction.y;
          inv_eye_dir.z = -1.0 * eye_ray->direction.z;
          normalize(&inv_eye_dir);

          /*視線ベクトルの逆ベクトルと法線ベクトルの内積*/
          vn_dot = dot(&inv_eye_dir, &intp.normal);
          if(vn_dot > 0){
            vector_t ref_dir; /*視線ベクトルの正反射ベクトル*/
            ray_t re_ray; /*正反射方向のレイ*/
            colorf_t re_col; /*正反射方向の色*/

            /*視線ベクトルの正反射ベクトルの計算*/
            ref_dir.x = 2.0 * vn_dot * intp.normal.x - inv_eye_dir.x;
            ref_dir.y = 2.0 * vn_dot * intp.normal.y - inv_eye_dir.y;
            ref_dir.z = 2.0 * vn_dot * intp.normal.z - inv_eye_dir.z;
            normalize(&ref_dir);

            /*正反射方向のレイの視点の計算*/
            re_ray.start.x = intp.position.x + EPSILON * ref_dir.x;
            re_ray.start.y = intp.position.y + EPSILON * ref_dir.y;
            re_ray.start.z = intp.position.z + EPSILON * ref_dir.z;

            re_ray.direction = ref_dir; /*レイの方向ベクトル*/
            re_col = *out_col; /*デフォルトで背景を入れておく*/
            recursive_raytrace(scene,&re_ray,&re_col,recursion_level+1); /*再帰呼出し*/

            /*完全鏡面反射光の計算*/
            col.r += shape->material.reflect_ref.r * re_col.r;
            col.g += shape->material.reflect_ref.g * re_col.g;
            col.b += shape->material.reflect_ref.b * re_col.b;
          } /*if(vn_dot > 0)*/
        } /*if(shape->material.type == MT_PERFECT_REF)*/

        /*反射・屈折する質感の場合*/
        else if(shape->material.type == MT_REFRACTION){
          vector_t inv_eye_dir; /*視線ベクトルの逆ベクトル*/
          vector_t eye_dir = eye_ray->direction; /*視線ベクトル*/
          vector_t n_dir = intp.normal; /*法線ベクトル*/
          float vn_dot; /*視線ベクトルの逆ベクトルと法線ベクトルの内積*/

          float eta_1 = scene->global_refraction_index; /*物質１の絶対屈折率:大気の屈折率で良い*/
          float eta_2 = shape->material.refraction_index; /*物質2の絶対屈折率：物質の屈折率で良い*/
          float eta_r; /*一時変数eta(r)*/
          vector_t re_dir,fe_dir; /*反射方向、屈折方向*/
          ray_t re_ray, fe_ray; /*反射レイ、屈折レイ*/
          colorf_t re_col,fe_col; /*反射光の輝度、屈折光の輝度*/
          float cos_theta1,cos_theta2; /*cos(theta1),cos(theta2)*/
          float rho_p,rho_s; /*rho偏光反射率、s偏光反射率*/
          float cr,ct; /*反射率、透過率*/
          float omega; /*一時変数omega*/

          normalize(&eye_dir); /*視線ベクトルは正規化されているとは限らないので正規化*/

          /*視線ベクトルの逆ベクトル*/
          inv_eye_dir.x = -eye_dir.x;
          inv_eye_dir.y = -eye_dir.y;
          inv_eye_dir.z = -eye_dir.z;

          /*内積の計算*/
          vn_dot = dot(&inv_eye_dir,&n_dir);

          if(vn_dot < 0){ /*物体面の裏側から進入した時*/
            /*法線ベクトルをひっくり返す*/
            n_dir.x = -n_dir.x;
            n_dir.y = -n_dir.y;
            n_dir.z = -n_dir.z;

            vn_dot = dot(&inv_eye_dir,&n_dir); /*内積を計算し直す*/

            /*屈折率１，２をひっくり返す*/
            float tmp = eta_1;
            eta_1 = eta_2;
            eta_2 = tmp;

          }/*if(vn_dot < 0)*/

          /*一時変数eta(r)の計算*/
          eta_r = eta_2 / eta_1;

          /*cos(theta1),cos(theta2)の計算*/
          cos_theta1 = vn_dot;
          cos_theta2 = sqrt(pow(eta_r,2)-(1-pow(cos_theta1,2)))/eta_r;

          /*一時変数omegaの計算*/
          omega = eta_r*cos_theta2 - cos_theta1;

          /*屈折方向ベクトルの計算*/
          fe_dir.x = (eye_dir.x - omega * n_dir.x)/eta_r;
          fe_dir.y = (eye_dir.y - omega * n_dir.y)/eta_r;
          fe_dir.z = (eye_dir.z - omega * n_dir.z)/eta_r;
          normalize(&fe_dir);

          /*正反射方向ベクトルの計算*/
          re_dir.x = 2.0*vn_dot*n_dir.x - inv_eye_dir.x;
          re_dir.y = 2.0*vn_dot*n_dir.y - inv_eye_dir.y;
          re_dir.z = 2.0*vn_dot*n_dir.z - inv_eye_dir.z;
          normalize(&re_dir);

          /*完全鏡面反射率、透過率の計算*/
          rho_p = (eta_r*cos_theta1 - cos_theta2)/(eta_r*cos_theta1 + cos_theta2);
          rho_s = -omega/(eta_r*cos_theta1 + cos_theta2);
          cr = (pow(rho_p,2)+pow(rho_s,2))/2.0;
          ct = 1-cr;

          /*正反射方向のレイの視点を計算する*/
          re_ray.start.x = intp.position.x + EPSILON * re_dir.x;
          re_ray.start.y = intp.position.y + EPSILON * re_dir.y;
          re_ray.start.z = intp.position.z + EPSILON * re_dir.z;
          re_ray.direction = re_dir;

          /*屈折方向のレイの視点を計算する*/
          fe_ray.start.x = intp.position.x + EPSILON * fe_dir.x;
          fe_ray.start.y = intp.position.y + EPSILON * fe_dir.y;
          fe_ray.start.z = intp.position.z + EPSILON * fe_dir.z;
          fe_ray.direction = fe_dir;

          re_col = fe_col = *out_col; /*デフォルトで背景色を入れる*/

          /*再帰呼出し（反射）*/
          recursive_raytrace(scene,&re_ray,&re_col,recursion_level+1);
          /*再帰呼出し（屈折）*/
          recursive_raytrace(scene,&fe_ray,&fe_col,recursion_level+1);

          /*完全鏡面反射・屈折光を計算する*/
          col.r += shape->material.reflect_ref.r * (cr * re_col.r + ct * fe_col.r);
          col.g += shape->material.reflect_ref.g * (cr * re_col.g + ct * fe_col.g);
          col.b += shape->material.reflect_ref.b * (cr * re_col.b + ct * fe_col.b);
        }/*else if(shape->material.type == MT_REFRACTION)*/


        ray_t shadow_ray; /*シャドウレイの定義*/
        shadow_ray.start.x = intp.position.x + EPSILON * light_dir.x;
        shadow_ray.start.y = intp.position.y + EPSILON * light_dir.y;
        shadow_ray.start.z = intp.position.z + EPSILON * light_dir.z;
        shadow_ray.direction = light_dir;

        /*交差判定を行う*/
        int res2;
        res2 = get_nearest_shape(scene, &shadow_ray, dl, 0, NULL, NULL); /*交差判定*/
        if(res2!=0){
          break;
        }

        nl_dot = dot(&(intp.normal),&light_dir); /*法線ベクトルと入射ベクトルの内積*/
        nl_dot = CLAMP(nl_dot,0,1);

        /*拡散反射光の計算*/
        col.r += shape->material.diffuse_ref.r * light->illuminance.r * nl_dot;
        col.g += shape->material.diffuse_ref.g * light->illuminance.g * nl_dot;
        col.b += shape->material.diffuse_ref.b * light->illuminance.b * nl_dot;

        if(nl_dot > 0){
          vector_t ref_dir; /*正反射ベクトル*/
          vector_t inv_eye_dir; /*視線ベクトルの逆ベクトル*/
          float vr_dot; /*視線ベクトルの逆ベクトルと正反射ベクトルの内積*/

          ref_dir.x = 2.0 * nl_dot * intp.normal.x - light_dir.x;
          ref_dir.y = 2.0 * nl_dot * intp.normal.y - light_dir.y;
          ref_dir.z = 2.0 * nl_dot * intp.normal.z - light_dir.z;

          normalize(&ref_dir);

          inv_eye_dir.x = -1.0 * eye_ray->direction.x;
          inv_eye_dir.y = -1.0 * eye_ray->direction.y;
          inv_eye_dir.z = -1.0 * eye_ray->direction.z;

          normalize(&inv_eye_dir);

          vr_dot = dot(&inv_eye_dir,&ref_dir);
          vr_dot = CLAMP(vr_dot,0,1);

          /*鏡面反射光の計算*/
          col.r += shape->material.specular_ref.r * light->illuminance.r * pow(vr_dot,shape->material.shininess);
          col.g += shape->material.specular_ref.g * light->illuminance.g * pow(vr_dot,shape->material.shininess);
          col.b += shape->material.specular_ref.b * light->illuminance.b * pow(vr_dot,shape->material.shininess);
        }/*if*/
      }/*for(i = 0; i < scene->num_lights; ++i)*/

      *out_col = col; /*色の出力*/
      return 1; /*交差があったので非ゼロを返す*/
    }/* if(res) */
  }/*else*/
  return 0;
}

int raytrace(const scene_t* scene, const ray_t *eye_ray, colorf_t *out_col){
  return recursive_raytrace(scene,eye_ray,out_col,0);
}/* int raytrace(const scene_t* scene, const ray_t *eye_ray, colorf_t *out_col) */

int intersection_test(const shape_t *shape, const ray_t* ray,
                      intersection_point_t* out_intp)
{
  if ( shape->type == ST_SPHERE )
    {
      const sphere_t *sph = &shape->data.sphere;
      vector_t pe_pc;
      float A,B,C,D;
      float t;

      pe_pc.x = ray->start.x - sph->center.x;
      pe_pc.y = ray->start.y - sph->center.y;
      pe_pc.z = ray->start.z - sph->center.z;

      A = dot(&ray->direction, &ray->direction);
      B = 2 * dot(&ray->direction, &pe_pc);
      C = dot(&pe_pc, &pe_pc) - SQR(sph->radius);

      D = SQR(B) - 4*A*C;

      t = -1.0f;
      if ( D == 0 )
        {
          t = -B / (2*A);
        }
      else if ( D > 0 )
        {
          float t1 = (-B + sqrt(D))/(2*A);
          float t2 = (-B - sqrt(D))/(2*A);

          if ( t1 > 0 ) t = t1;
          if ( t2 > 0 && t2 < t ) t = t2;
        }

      if ( t > 0 )
        {
          if ( out_intp )
            {
              out_intp->distance = t;

              out_intp->position.x = ray->start.x + t * ray->direction.x;
              out_intp->position.y = ray->start.y + t * ray->direction.y;
              out_intp->position.z = ray->start.z + t * ray->direction.z;

              out_intp->normal.x = out_intp->position.x - sph->center.x;
              out_intp->normal.y = out_intp->position.y - sph->center.y;
              out_intp->normal.z = out_intp->position.z - sph->center.z;
              normalize(&out_intp->normal);
            }

          return 1;
        }
      else
        {
          return 0;
        }
    }
  else if ( shape->type == ST_PLANE )
    {
      const plane_t *pln = &shape->data.plane;
      float dn_dot = dot(&ray->direction, &pln->normal);

      if ( dn_dot != 0 )
        {
          vector_t s_p;
          float t;

          s_p.x = ray->start.x - pln->position.x;
          s_p.y = ray->start.y - pln->position.y;
          s_p.z = ray->start.z - pln->position.z;

          t = -dot(&s_p, &pln->normal) / dn_dot;

          if ( t > 0 )
            {
              if ( out_intp )
                {
                  out_intp->distance = t;

                  out_intp->position.x = ray->start.x + t * ray->direction.x;
                  out_intp->position.y = ray->start.y + t * ray->direction.y;
                  out_intp->position.z = ray->start.z + t * ray->direction.z;

                  out_intp->normal = pln->normal;
                }

              return 1;
            }
          else
            {
              return 0;
            }
        }
    }
  return 0;
}/* int intersection_test(const shape_t *shape, const ray_t* ray,  */

int get_nearest_shape(const scene_t* scene, const ray_t *ray, float max_dist, int exit_once_found,
                      shape_t **out_shape, intersection_point_t *out_intp)
{
  size_t i;
  shape_t *nearest_shape = NULL;
  intersection_point_t nearest_intp;
  nearest_intp.distance = max_dist;

  for(i = 0; i < scene->num_shapes; ++i)
    {
      int res;
      intersection_point_t intp;

      res = intersection_test(&scene->shapes[i], ray, &intp);

      if ( res && intp.distance < nearest_intp.distance )
        {
          nearest_shape = &scene->shapes[i];
          nearest_intp = intp;
          if ( exit_once_found ) break;
        }
    }/* for */

  if ( nearest_shape )
    {
      if ( out_shape )
        *out_shape = nearest_shape;
      if ( out_intp )
        *out_intp = nearest_intp;

      return 1;
    }
  else
    {
      return 0;
    }
}


void init_shape(shape_t* shape, shape_type st, ...)
{
  va_list args;
  va_start(args, st);

  shape->type = st;
  if ( st == ST_SPHERE )
    {
      sphere_t* sph = &shape->data.sphere;

      sph->center.x = va_arg(args, double);
      sph->center.y = va_arg(args, double);
      sph->center.z = va_arg(args, double);
      sph->radius   = va_arg(args, double);
    }
  else if ( st == ST_PLANE )
    {
      plane_t* plane = &shape->data.plane;

      plane->position.x = va_arg(args, double);
      plane->position.y = va_arg(args, double);
      plane->position.z = va_arg(args, double);

      plane->normal.x = va_arg(args, double);
      plane->normal.y = va_arg(args, double);
      plane->normal.z = va_arg(args, double);
    }
  else
    {
      fprintf(stderr, "init_shape : unknown shape type.\n");
      abort();
    }

  va_end(args);
}

void init_material(material_t* mat,
		   float ambR, float ambG, float ambB,
		   float difR, float difG, float difB,
		   float speR, float speG, float speB,
		   float shns, material_type mt, float refR, float refG, float refB, float refi)
{
  SET_COLOR(mat->ambient_ref,  ambR, ambG, ambB);
  SET_COLOR(mat->diffuse_ref,  difR, difG, difB);
  SET_COLOR(mat->specular_ref, speR, speG, speB);
  mat->shininess = shns;
  mat->type = mt;
  SET_COLOR(mat->reflect_ref,  refR, refG, refB)
  mat->refraction_index = refi;
}

void init_light(light_t* light, light_type lt,
		float vx, float vy, float vz,
		float illR, float illG, float illB)
{
  light->type = lt;
  SET_VECTOR(light->vector, vx, vy, vz);
  SET_COLOR(light->illuminance, illR, illG, illB);
}
