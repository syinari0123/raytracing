#include <stdio.h>
#include <math.h>
#include "vector_utils.h"

#define WIDTH 512
#define HEIGHT 512

int main()
{
  int y,x;                            /* スクリーン座標 */
  unsigned char r,g,b;                /* 画素値(RGB) */

  vector_t eye_pos    = {0.0f, 0.0f,-5.0f};/* 視点位置 */
  vector_t sphere_pos = {0.0f, 0.0f,5.0f};/* 球の中心 */
  float sphere_r      = 1.0 ;/* 球の半径 */
  vector_t pw;                        /* 三次元空間でのスクリーン座標 */

  printf("P3\n");
  printf("%d %d\n", WIDTH, HEIGHT);
  printf("255\n");

  pw.z = 0; /* スクリーンのz座標は常にゼロ */

  for(y = 0; y < HEIGHT; ++y)
    {
      pw.y = -2.0 * y / (HEIGHT -1.0) + 1.0;
      for(x = 0; x < WIDTH; ++x)
        {
          vector_t eye_dir; /* 視線方向ベクトル */
          vector_t s_c;     /* 視点 - 球の中心 */
          float A,B,C,D;    /* 二次方程式Ax^2+Bx+C=0および判別式D */

          pw.x = 2.0 * x / (WIDTH -1.0) -1.0;

          /* レイと球の交差判定 */
          eye_dir.x = pw.x - eye_pos.x;
          eye_dir.y = pw.y - eye_pos.y;
          eye_dir.z = pw.z - eye_pos.z;
          s_c.x = eye_pos.x - sphere_pos.x;
          s_c.y = eye_pos.y - sphere_pos.y;
          s_c.z = eye_pos.z - sphere_pos.z;

          /* 二次方程式の係数(At^2 + Bt + C = 0) */
          A = squared_norm(&eye_dir);
          B = 2.0 * dot(&eye_dir,&s_c);
          C = squared_norm(&s_c) - pow(sphere_r,2);

          D = SQR(B) - 4*A*C;

          r = 0; g = 0; b = 255;
          if ( D >= 0 )
            {
              r = 255; g = 0; b = 0;
            }/* if */

          printf("%d\t%d\t%d\n",r,g,b);

        }/* for */
    }/* for */

  return 0;

}/* int main() */
