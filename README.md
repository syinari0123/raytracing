# メディアコンテンツ特別講義２レポート用 ソースコード
東京電機大学の工学部第二部情報通信工学実験　グラフィックス応用テキスト  
https://www.vcl.jp/~kanazawa/raytracing/?page_id=1132  
の講義ページを参考にしながら実装を行いました。  
  
・1_first_sample  
視線の物体との交差判定に関する実装を行った。以下のように実行すれば良い。  
`$ gcc -std=c89 -Wall 1_first_sample.c vector_utils.c -lm -o 1_first_sample`  
`$ ./1_first_sample > 1_first_sample.ppm`  

・2_simple_shading  
Phongの反射モデルに関する実装を行った。以下のように実行すれば良い。  
`$ make scene2.ppm`  

・3_specular_reflect  
完全鏡面反射に関する実装を行った。以下のように実行すれば良い。  
`$ make scene4.ppm`  

・4_refraction  
光の屈折に関する実装を行った。以下のように実行すれば良い。  
`$ make scene5.ppm` 
